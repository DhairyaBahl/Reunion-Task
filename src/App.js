import { useEffect } from 'react';
import './App.scss';
import Header from './components/Header/Header';
import Main from './components/Main/Main';

function App() {
  useEffect(() => {
    console.log('App Inititated ...');
    console.log('Source Maps are Enabled 🧸, Feel free to check code quality 🤓');
  }, [])

  return (
    <div className = "app">
      <Header/>
      <Main/>
    </div>
  );
}

export default App;