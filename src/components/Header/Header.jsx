import React from 'react'
import './Header.scss';
import Logo from '../../assets/logo.png'

function Header() {
  return (
    <>
      <div className='header'>
          <div className="headerContent">
              <div className="headerContentLeft">
                  <img
                      src = {Logo}
                      alt = "logo"
                      className="headerLogo"
                  />
              </div>
              <div className="headerContentRight"></div>
          </div>
      </div>
      <div className='dummyHeader'/>
    </>
  )
}

export default Header