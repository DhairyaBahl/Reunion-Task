import React from 'react'

function FilterOption({defaultValue, values, onChange}) {
    function handler(event) {
        onChange(event.target.value)
    }

    if(defaultValue === 'Prices') {
        return (
            <select className="filterCriteria" onChange={handler}>
                <option selected value={defaultValue}>{defaultValue}</option>
                {values.map(value => <option value={value}>{value} - {value + 20000}</option>)}
            </select>
        )
    }

  return (
    <select className="filterCriteria" onChange={handler}>
        <option selected value={defaultValue}>{defaultValue}</option>
        {values.map(value => <option value={value}>{value}</option>)}
    </select>
  )
}

export default FilterOption