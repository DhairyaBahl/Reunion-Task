import React, { useEffect, useState } from 'react'
import './Main.scss';
import FilterOption from '../FilterOption/FilterOption';
import RentalProperties from '../../data/rentalProperties.json';
import CardItem from '../CardItem/CardItem';

function Main() {
  const [locations, setLocations] = useState([]);
  const [types, setTypes] = useState([]);
  const [prices, setPrices] = useState([]);
  const [startingDates, setStartingDates] = useState([]);
  const [items, setItems] = useState([]);
  const [originalItems, setOriginalItems] = useState([]);

  const [location, setLocation] = useState("Location");
  const [date, setDate] = useState('Available Starting Date');
  const [price, setPrice] = useState('Prices');
  const [type, setType] = useState('Property Type');

  function preCompute() {
    let locations = new Set(), 
    availableStartingDates = new Set(),
    propertyTypes = new Set(),
    pricesMap = {};

    RentalProperties.forEach(property => {
      locations.add(property.location);
      availableStartingDates.add(property.availableStartingDate);
      propertyTypes.add(property.propertyType);

      let currPrice = parseInt(property.price, 10);

      if(currPrice % 20000 === 0) {
        let prevPrice = currPrice - 20000;
        if(!pricesMap[prevPrice]) pricesMap[prevPrice] = [];
        pricesMap[prevPrice].push(currPrice);
      }
      else {
        let prevPrice = currPrice - (currPrice % 20000)
        if(!pricesMap[prevPrice]) pricesMap[prevPrice] = [];
        pricesMap[prevPrice].push(currPrice);
      }
    })

    setLocations(Array.from(locations));

    availableStartingDates = Array.from(availableStartingDates);

    let sortedAvailableStartingDates = availableStartingDates.sort((a, b) => {
      const aParts = a.split('-'),
            bParts = b.split('-');
      
      const aDate = new Date(aParts[2], aParts[1] - 1, aParts[0]),
            bDate = new Date(bParts[2], bParts[1] - 1, bParts[0]);
        
      return aDate - bDate;
    });

    setPrices(Object.keys(pricesMap).map(price => parseInt(price, 10)).sort((a, b) => a - b));
    setStartingDates(Array.from(sortedAvailableStartingDates));
    setTypes(Array.from(propertyTypes));
    setOriginalItems(RentalProperties);
  }

  useEffect(() => {
    preCompute();
  }, []);

  useEffect(() => {
    let newItems = originalItems;

    if(location !== "Location") {
      newItems = newItems.filter(item => item.location === location);
    }

    if(date !== 'Available Starting Date') {
      newItems = newItems.filter(item => item.availableStartingDate === date);
    }

    if(price !== 'Prices') {
      newItems = newItems.filter(item => {
        let currPrice = parseInt(item.price, 10);
        let prevPrice;
        if(currPrice % 20000 === 0) {
          prevPrice = currPrice - 20000;
        }
        else {
          prevPrice = currPrice - (currPrice % 20000);
        }

        return prevPrice === parseInt(price, 10);
      });

    }
    
    if(type !== 'Property Type') {
      newItems = newItems.filter(item => item.propertyType === type);
    }

    setItems(newItems);
  }, [location, date, price, type, originalItems]);

  return (
    <div className='main'>
        <div className="mainContent">
            <div className="mainContentHeader">
              <div className="mainContentHeaderLeft">
                Search Properties for Rent
              </div>
              <div className="mainContentHeaderRight">
              {
                /*
                  <div className="mainContentHeaderRightContainer">
                    <input 
                      type="text"
                      placeholder="Search"
                      className="mainContentHeaderRightInput"
                    />
                    <img 
                      src={SearchIcon} 
                      alt="Search" 
                      className="mainContentHeaderRightIcon" 
                    />
                  </div>
                */
              } 
              </div>
            </div>
            <div className="mainContentFilterContainer">
              <FilterOption
                defaultValue = {'Location'}
                values = {locations}
                onChange = {setLocation}
              />

              <FilterOption
                defaultValue = {'Available Starting Date'}
                values = {startingDates}
                onChange = {setDate}
              />

              <FilterOption
                defaultValue={'Prices'}
                values={prices}
                onChange={setPrice}
              />

              <FilterOption
                defaultValue={'Property Type'}
                values={types}
                onChange={setType}
              />

              <button className="mainContentFilterButton">
                Filter
              </button>
            </div>
            <div className="mainContentList">
              {
                items.map(item => <CardItem {...item}/>)
              }
            </div>
        </div>
    </div>
  )
}

export default Main;