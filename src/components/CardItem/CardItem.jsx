import React from 'react'
import './CardItem.scss'

function CardItem({imageUrl, price, location, availableStartingDate, propertyType}) {
  return (
    <div className='cardItem'>
        <img src={imageUrl} alt=""/>
        <div className="cardBody">
            <div>
                <span className="price">Rs {price}</span>
                <span className="perMonth">/month</span>
            </div>
            <div className='location'>
                {location}
            </div>
            <div className='dateOfAvailability'>
                Available from {availableStartingDate}
            </div>
            <div className = 'typeOfProperty'>
                {propertyType}
            </div>
        </div>
    </div>
  )
}

export default CardItem